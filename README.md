# How this task will be evaluated
## We expect your solution to the task to meet the following criteria:

- You have applied 2 background images in the styles
- You have commented the header section in HTML
- You have not used the style attribute in HTML
- You have written styles to one CSS file or several separate CSS files
- Your markup matches the specific image in the task